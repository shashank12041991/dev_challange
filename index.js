
// This is not really required, but means that changes to index.html will cause a reload.
require('./site/index.html')
// Apply the styles in style.css to the page.
require('./site/style.css')
// if you want to use es6, you can do something like
require('./es6/myEs6code')
// here to load the myEs6code.js file, and it will be automatically transpiled.

// Change this to get detailed logging from the stomp library
global.DEBUG = false

const url = "ws://localhost:8011/stomp"
const client = Stomp.client(url)
client.debug = function(msg) {
  if (global.DEBUG) {
    console.info(msg)
  }
}

const hashtable = {} //maintains unique currency name with assigned index value
const bidArr = {}

function showTable(outputArr){
  //Create a HTML Table element.
    var table = document.createElement("table");
    table.border = "0";

    //Get the count of columns.
    var columnCount = Object.keys(outputArr[0]).length;

    //Add the header row.
    var row = table.insertRow(-1);
    for (let i = 0; i < columnCount; i++) {
        var headerCell = document.createElement("th");
        headerCell.innerHTML = Object.keys(outputArr[0])[i];
        row.appendChild(headerCell);
    }

    //Add the data rows.
    for (let i = 1; i < outputArr.length; i++) {
        row = table.insertRow(-1);
        for (let j = 0; j < columnCount; j++) {
            var cell = row.insertCell(-1);
            cell.innerHTML = j!=0?Number(outputArr[i][Object.keys(outputArr[i])[j]]).toFixed(3):outputArr[i][Object.keys(outputArr[i])[j]];

            if(j===columnCount-1){
              cell.innerHTML="";
              const sparkElement = document.createElement('span')
              const sparkline = new Sparkline(sparkElement)
              sparkline.draw(outputArr[i][Object.keys(outputArr[i])[j]])
              cell.appendChild(sparkElement)
            }
        }
    }
    var dvTable = document.getElementById("wrapper");
    dvTable.innerHTML = "";
    dvTable.appendChild(table);
}

function connectCallback() {
  document.getElementById('stomp-status').innerHTML = "It has now successfully connected to a stomp server serving price updates for some foreign exchange currency pairs."
  client.subscribe('/fx/prices', callback, null);
}

client.connect({}, connectCallback, function(error) {
  alert(error.headers.message)
})
function compare(a,b){
  let attr = 'lastChangeBid'
  return a[attr]-b[attr];
}
function objToArr(obj)
{
  tempArr=[];
  for(let key in obj)
    tempArr.push(obj[key])
  return tempArr;
}
function callback(message) {
    // called when the client receives a STOMP message from the server
    if (message.body) {
      const msg_body = JSON.parse(message.body);

      hashtable[msg_body.name] = msg_body;
      if(!bidArr[msg_body.name])
        bidArr[msg_body.name]= new Array();

      if( bidArr[msg_body.name].length>30)
        bidArr[msg_body.name].shift();

      const midprice = (msg_body.bestBid + msg_body.bestAsk)/2;
      bidArr[msg_body.name].push(Number(midprice).toFixed(2))
      hashtable[msg_body.name]['midprice (last 30 sec)'] = bidArr[msg_body.name];

      let baseArr=[]
      baseArr = objToArr(hashtable);
      baseArr = baseArr.sort(compare);
      showTable(baseArr.reverse());
    } else {
      //alert("got empty message");
    }
  }
